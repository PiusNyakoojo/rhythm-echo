
import * as tf from '@tensorflow/tfjs'

// Original source: https://github.com/mwdchang/tfjs-gan

// Input params
const BATCH = 200
const SIZE = 28
const INPUT_SIZE = SIZE * SIZE
const SEED_SIZE = 40
const SEED_STD = 3.5
const ONES = tf.ones([BATCH, 1])
const ONES_PRIME = tf.ones([BATCH, 1]).mul(tf.scalar(0.98))
const ZEROS = tf.zeros([BATCH, 1])

// Generator and Discriminator params
const DISCRIMINATOR_LEARNING_RATE = 0.025
const GENERATOR_LEARNING_RATE = 0.025
const dOptimizer = tf.train.sgd(DISCRIMINATOR_LEARNING_RATE)
const gOptimizer = tf.train.sgd(GENERATOR_LEARNING_RATE)

// Helper functions
const varInitNormal = (shape: number[], mean: number = 0, std: number = 0.1) => tf.variable(tf.randomNormal(shape, mean, std))
const varLoad = (shape: number[], data: any) => tf.variable(tf.tensor(shape, data))
const seed = (s: number = BATCH) => tf.randomNormal([s, SEED_SIZE], 0, SEED_STD)

const g1w = varInitNormal([SEED_SIZE, 140])
const g1b = varInitNormal([140])
const g2w = varInitNormal([140, 80])
const g2b = varInitNormal([80])
const g3w = varInitNormal([80, INPUT_SIZE])
const g3b = varInitNormal([INPUT_SIZE])

const d1w = varInitNormal([INPUT_SIZE, 200])
const d1b = varInitNormal([200])
const d2w = varInitNormal([200, 90])
const d2b = varInitNormal([90])
const d3w = varInitNormal([90, 1])
const d3b = varInitNormal([1])

function gen (xs: tf.Tensor): tf.Tensor {
  const l1 = tf.leakyRelu(xs.matMul(g1w).add(g1b))
  const l2 = tf.leakyRelu(l1.matMul(g2w).add(g2b))
  const l3 = tf.tanh(l2.matMul(g3w).add(g3b))
  return l3
}

function disReal (xs: tf.Tensor): tf.Tensor[] {
  const l1 = tf.leakyRelu(xs.matMul(d1w).add(d1b))
  const l2 = tf.leakyRelu(l1.matMul(d2w).add(d2b))
  const logits = l2.matMul(d3w).add(d3b)
  const output = tf.sigmoid(logits)
  return [logits, output]
}

function disFake (xs: tf.Tensor): tf.Tensor[] {
  return disReal(gen(xs))
}

function sigmoidCrossEntropyWithLogits (target: any, output: any) {
  return tf.tidy(() => {
    const maxOutput = tf.maximum(output, tf.zerosLike(output))
    const outputXTarget = tf.mul(output, target)
    const sigmoidOutput = tf.log(tf.add(tf.scalar(1.0), tf.exp(tf.neg(tf.abs(output)))))
    const result = tf.add(tf.sub(maxOutput, outputXTarget), sigmoidOutput)
    return result
  })
}

async function trainBatch (realBatch: tf.Tensor, fakeBatch: tf.Tensor): Promise<tf.Tensor[]> {
  const dcost = dOptimizer.minimize(() => {
    const [logitsReal, outputReal] = disReal(realBatch)
    const [logitsFake, outputFake] = disFake(fakeBatch)

    const lossReal = sigmoidCrossEntropyWithLogits(ONES_PRIME, logitsReal)
    const lossFake = sigmoidCrossEntropyWithLogits(ZEROS, logitsFake)
    return lossReal.add(lossFake).mean()
  }, true, [d1w, d1b, d2w, d2b, d3w, d3b])

  await tf.nextFrame()

  const gcost = gOptimizer.minimize(() => {
    const [logitsFake, outputFake] = disFake(fakeBatch)

    const lossFake = sigmoidCrossEntropyWithLogits(ONES, logitsFake)
    return lossFake.mean()
  }, true, [g1w, g1b, g2w, g2b, g3w, g3b])

  await tf.nextFrame()

  return [dcost!, gcost!]
}

class TensorflowGANProvider {

}

export {
  TensorflowGANProvider
}
